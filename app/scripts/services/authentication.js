'use strict';

/**
 * @ngdoc service
 * @name inflightDemoApp.authentication
 * @description
 * # authentication
 * Service in the inflightDemoApp.
 */
angular.module('inflightDemoApp')
  .service('authentication', function ($rootScope, $sessionStorage, $state) {
    var self = this;

    var authorizedUsers = {
      admin: 'password',
      joe: 'qwerty'
    };

    self.isAuthenticated = function () {
      return $sessionStorage.isAuthenticated;
    };

    self.authenticate = function (username, password) {
      $sessionStorage.isAuthenticated = authorizedUsers[username] === password;
      if ($sessionStorage.isAuthenticated) {
        $state.transitionTo('home');
      }
      $rootScope.$emit('$authentication-event', self.isAuthenticated());
    };

    self.logout = function () {
      $sessionStorage.isAuthenticated = false;
      $rootScope.$emit('$authentication-event', self.isAuthenticated());
      $state.transitionTo('login');
    };
  });
