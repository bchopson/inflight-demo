'use strict';

/**
 * @ngdoc overview
 * @name inflightDemoApp
 * @description
 * # inflightDemoApp
 *
 * Main module of the application.
 */
angular
  .module('inflightDemoApp', [
    'ngResource',
    'ui.router',
    'ngStorage'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl as login',
        authenticate: false
      })
      .state('home', {
        url: '/',
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl as home',
        authenticate: true
      })
      .state('about', {
        url: '/about',
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl as about',
        authenticate: true
      })
      .state('contact', {
        url: '/contact',
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl as contact',
        authenticate: true
      });

      $urlRouterProvider.otherwise('/');
  })
  .run(function ($rootScope, $state, authentication) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (toState.authenticate && !authentication.isAuthenticated()) {
        $state.transitionTo('login');
        event.preventDefault();
      }
    });
  })
;
