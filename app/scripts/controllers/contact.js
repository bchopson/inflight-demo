'use strict';

/**
 * @ngdoc function
 * @name inflightDemoApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the inflightDemoApp
 */
angular.module('inflightDemoApp')
  .controller('ContactCtrl', function () {
    var self = this;
    self.messageSent = false;

    self.submit = function () {
      self.messageSent = true;
      Object.keys(self.data).forEach(function (key) {
        self.data[key] = '';
      });
    };
  });
