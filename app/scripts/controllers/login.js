'use strict';

/**
 * @ngdoc function
 * @name inflightDemoApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the inflightDemoApp
 */
angular.module('inflightDemoApp')
  .controller('LoginCtrl', function (authentication) {
    var self = this;
    self.authFailed = false;

    self.submit = function () {
      authentication.authenticate(self.data.username, self.data.password);
      self.authFailed = !authentication.isAuthenticated();
    };
  });
