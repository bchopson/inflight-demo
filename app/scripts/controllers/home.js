'use strict';

/**
 * @ngdoc function
 * @name inflightDemoApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the inflightDemoApp
 */
angular.module('inflightDemoApp')
  .controller('HomeCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
