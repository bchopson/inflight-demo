'use strict';

/**
 * @ngdoc function
 * @name inflightDemoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the inflightDemoApp
 */
angular.module('inflightDemoApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
