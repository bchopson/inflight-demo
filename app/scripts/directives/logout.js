'use strict';

/**
 * @ngdoc directive
 * @name inflightDemoApp.directive:logout
 * @description
 * # logout
 */
angular.module('inflightDemoApp')
  .controller('LogoutCtrl', function ($rootScope, authentication) {
    var self = this;
    self.isAuthenticated = authentication.isAuthenticated();

    self.logout = function () {
      authentication.logout();
    };

    $rootScope.$on('$authentication-event', function (event, args) {
      self.isAuthenticated = args;
    });
  })
  .directive('logout', function () {
    return {
      templateUrl: 'views/logout.html',
      restrict: 'E',
      controller: 'LogoutCtrl',
      controllerAs: 'logout'
    };
  });
